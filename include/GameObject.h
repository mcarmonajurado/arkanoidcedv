﻿#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <OGRE/Ogre.h>

/*
 * This class is intended to be used for parent class of the displayed objects.
 */
class GameObject
{
public:

    GameObject(const Ogre::String& name = "", const Ogre::String& meshName = "", const Ogre::String& materialName = "")
    {
        _name = name;
        _sceneNode = 0;
        _meshName = meshName;
        _materialName = materialName;
    }

    virtual ~GameObject() { }

    virtual void addToScene(Ogre::SceneManager* scene_manager) = 0;
    virtual void removeFromScene() = 0;

    /*
     * Used for updating objects logic.
     * If this method returns false this object should be destroyed.
     * A child class should redefine it.
     */
    virtual bool updateInnerStatus(Ogre::Real time_step) { return true; }

    const Ogre::String& getName() const { return _name; }

    Ogre::SceneNode* getSceneNode() const { return _sceneNode; }

    static const int SCALE = 100;

protected:
    Ogre::String _name;
    Ogre::SceneNode * _sceneNode;
    Ogre::String _meshName;
    Ogre::String _materialName;
};

#endif
