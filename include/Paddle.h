#ifndef PADDLE_H
#define PADDLE_H

#include <OGRE/Ogre.h>
#include "GameObject.h"

class Paddle : public GameObject
{
public:
    Paddle();
    Paddle(Ogre::Real length);
    Paddle(Ogre::Real leftLimit, Ogre::Real rightLimit);
    ~Paddle();

    void addToScene(Ogre::SceneManager* scene_manager);
    void removeFromScene();

    bool updateInnerStatus(Ogre::Real time_step);

    void setSpeed(Ogre::Real speed) { _speedX = speed; }

    const Ogre::Real getSpeed() const { return _speedX; }

    void SetLength(Ogre::Real length)
    {
        _length = length;
        Ogre::Vector3 new_scale = _sceneNode->getScale();
        new_scale.x = length / SCALE;
        _sceneNode->setScale( new_scale );
    }

    const Ogre::Real getLength() const { return _length; }

private:
    Ogre::Real _length;
    Ogre::Real _speedX;
    Ogre::Real _leftLimit;
    Ogre::Real _rightLimit;

    static const Ogre::Vector3 STARTING_POSITION;
    static const Ogre::Vector3 STARTING_SIZE;
};

#endif // PADDLE_H
