#ifndef LEVEL_H
#define LEVEL_H

#include <OGRE/Ogre.h>
#include <list>

#include "Brick.h"
//class Brick;

typedef std::list<Brick*> BrickList;

/*
 * This class is intented to be use as interface for level class.
 * Each level should implements these methods.
 */
class Level
{
protected:
    BrickList _bricksList;// List of bricks of this level
    int _life;
    int _brokenBricks;

public:
    /* This method creates a level of type 1 */
    virtual void createLevel(Ogre::SceneManager *scene_manager) = 0;
    /* This method creates a level of type indicated by the parameter */
    virtual void createLevel(Ogre::SceneManager *scene_manager, int level) = 0;

    BrickList& getLevelBrickList(){ return _bricksList; }

    int getLife() { return _life; }
    void setLife(int life) { _life = life; }

    int getBrokenBricks() { return _brokenBricks; }
    void setBrokenBricks(int brokenBricks) { _brokenBricks = brokenBricks; }
};

#endif
