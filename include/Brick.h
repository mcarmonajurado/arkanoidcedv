#ifndef BRICK_H
#define BRICK_H

#include "GameObject.h"

#include <OGRE/Ogre.h>
//#include "Ball.hpp"

class Ball;

class Brick: public GameObject{

private:
    Ogre::Vector3 _pos;
    Ogre::Vector3 _size;
    int _hardness;//how many hits can a brick resist

    static unsigned long int brikCounterName;

    //we prevent the usage of the default and copy constructor
    Brick(){}
    Brick(const Brick&){}

public:
    Brick(const Ogre::Vector3 &position, const Ogre::Vector3 &size, int hardness);
    Brick(const Ogre::Vector3 &position, const Ogre::Vector3 &size, int hardness, const Ogre::String& materialName);
    //~Brick();

    void addToScene(Ogre::SceneManager* scene_manager);
    void removeFromScene();

    int onHitBall(Ball *b);//when the brick is hit by the ball
    bool updateInnerStatus(Ogre::Real time_step);//returns false when hardness = 0

};

#endif
