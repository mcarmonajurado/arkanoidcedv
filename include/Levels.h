#ifndef LEVELS_H
#define LEVELS_H

#include <OGRE/Ogre.h>

#include "Level.h"

class Levels : public Level
{
public:
    Levels() {}
    void createLevel(Ogre::SceneManager *scene_manager);
    void createLevel(Ogre::SceneManager *scene_manager, int level);
    static const int HIGHEST_LEVEL = 2;
private:
    void createLevel1(Ogre::SceneManager *scene_manager);
    void createLevel2(Ogre::SceneManager *scene_manager);
};

#endif // LEVELS_H
