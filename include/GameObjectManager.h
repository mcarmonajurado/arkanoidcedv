#ifndef GAME_OBJECT_MANAGER_HPP
#define GAME_OBJECT_MANAGER_HPP

#include <list>
#include <algorithm>
#include <OGRE/Ogre.h>

#include "GameObject.h"

typedef std::list<GameObject*> GameObjectList;// List of game objects

/*
 * Manage all game objects
 */
class GameObjectManager : public Ogre::Singleton<GameObjectManager>
{
public:
    GameObjectManager();
    ~GameObjectManager();

    void addObjectToCollection(GameObject* game_object)
    {
        _gameObjectList.push_back(game_object);
    }

    /* Here we update the status of every object in the game */
    void update(Ogre::Real time_step);

    void destroyAll();

    bool isEmpty(){ return _gameObjectList.empty();}

private:
    GameObjectList _gameObjectList;
};

#endif
