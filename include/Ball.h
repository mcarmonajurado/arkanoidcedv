#ifndef BALL_H
#define BALL_H

#include <OGRE/Ogre.h>
#include "GameObject.h"
//#include "Paddle.hpp"
//#include "Brick.hpp"

class Brick;
class Paddle;

class Ball : public GameObject
{
public:
    Ball();
    ~Ball();

    /* Constructor based on the position*/
    Ball(const Ogre::Vector3 &pos, const Ogre::Vector3 &spd, int hardness);

    void addToScene(Ogre::SceneManager* scene_manager);
    void removeFromScene();

    virtual bool updateInnerStatus(Ogre::Real time_step)
    {
//        Ogre::LogManager::getSingletonPtr()->logMessage("=======================");
//        Ogre::LogManager::getSingletonPtr()->logMessage("Ball::updateInnerStatus");
//        Ogre::LogManager::getSingletonPtr()->logMessage("=======================");

        _oldPosition = _sceneNode->getPosition();
        _sceneNode->translate(_speed * time_step);
        return true;
    }

    int getHardness() { return _hardness; }

    virtual void onHitBrick(Brick *brick);
    virtual void onHitWall();
    virtual void onHitPaddle(Paddle *pad);
    virtual void onHitCeiling();

    const Ogre::String& getName() const { return _name; }

    void setSpeed (const Ogre::Vector3 & speed) { _speed = speed; }
    Ogre::Vector3 getSpeed () { return _speed; }

private:
    bool sideCollision(const Ogre::Vector3& rectangle_position, const Ogre::Vector3& rectangle_size);
    static const Ogre::Vector3 STARTING_POSS;
    static const Ogre::Vector3 STARTING_SPEED;
    static const Ogre::Real RADIUS;

    Ogre::Vector3 _speed;
    Ogre::Vector3 _startingPosition;
    Ogre::Vector3 _oldPosition;
    int _hardness;
    bool _collided;
};

#endif // BALL_H
