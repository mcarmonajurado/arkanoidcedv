#ifndef WINNINGSTATE_H
#define WINNINGSTATE_H

#include <OGRE/Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

class WinningState : public Ogre::Singleton<WinningState>, public GameState
{
public:
    WinningState() {}

    void enter ();
    void exit ();
    void pause ();
    void resume ();

    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);
    bool frameRenderingQueued (const Ogre::FrameEvent& evt);

    // Heredados de Ogre::Singleton.
    static WinningState& getSingleton ();
    static WinningState* getSingletonPtr ();

protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneMgr;
    Ogre::Viewport* _viewport;
    Ogre::Camera* _camera;

    bool _exitGame;
};

#endif // WINNINGSTATE_H
