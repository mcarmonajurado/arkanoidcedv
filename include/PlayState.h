/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#include <OGRE/Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

#include "GameObjectManager.h"

#include "Level.h"
#include "Levels.h"
#include "Ball.h"
#include "Paddle.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
public:
    PlayState () {}

    void enter ();
    void exit ();
    void pause ();
    void resume ();

    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);
    bool frameRenderingQueued (const Ogre::FrameEvent& evt);

    // Heredados de Ogre::Singleton.
    static PlayState& getSingleton ();
    static PlayState* getSingletonPtr ();

    void createOverlay();

protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneMgr;
    Ogre::Viewport* _viewport;
    Ogre::Camera* _camera;

    bool _exitGame;

private:
    void saveRecord();
    void checkCollisions();
    void checkBrickCollisions();
    void checkPaddleCollisions();
    void checkBordersCollisions();
    bool circleCollidesRectangle(const Ogre::Vector3& circle_position, Ogre::Real circle_radius,
                                 const Ogre::Vector3& rectangle_position, const Ogre::Vector3& rectangle_size);
    Paddle* _paddle;
    Ball* _ball;

    bool _paddleCollided; // Disable momentarily collision for paddle

    static const Ogre::Real PADDLE_VELOCITY;
    static const Ogre::Real	LENGTH_PENALTY;

    static const Ogre::Real LEFT_WALL;
    static const Ogre::Real RIGHT_WALL;
    static const Ogre::Real CEILING;
    static const Ogre::Real FLOOR;

    GameObjectManager* _gameObjectManager;
    Levels* _level;

    std::vector<std::string> & split(const std::string &s, char delim, std::vector<std::string> &elems);
    std::vector<std::string> split(const std::string &s, char delim);
};

#endif
