#include "PauseState.h"
#include <iostream>
#include <fstream>

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

static const Ogre::ColourValue BACKGROUND_COLOUR = Ogre::ColourValue(0.5, 1.0, 0.0);

const Ogre::String OVERLAY_NAME = "Records";
static const Ogre::String LEVEL_BROKEN_BRICKS_SEPARATOR = "              ";
static const Ogre::String BROKEN_BRICKS_VAUS = "               ";

void
PauseState::enter ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Entrando en PauseState");
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");

    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    // Nuevo background colour.
    _viewport->setBackgroundColour(BACKGROUND_COLOUR);

    _exitGame = false;

    createOverlay();
}

void
PauseState::exit ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Saliendo de PauseState");
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");

    _overlayManager->getByName(OVERLAY_NAME)->hide();
}

void
PauseState::pause ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Pausando PauseState");
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");
}

void
PauseState::resume ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Continuando PauseState");
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
}

bool
PauseState::frameStarted
(const Ogre::FrameEvent& evt)
{
    // Gestion del overlay
    manageOverlay() ;
    return true;
}

bool
PauseState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
PauseState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    return true;
}

void
PauseState::keyPressed
(const OIS::KeyEvent &e) {
    // Tecla p --> Estado anterior.
    if (e.key == OIS::KC_P) {
        popState();
    }
}

void
PauseState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
PauseState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
PauseState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
PauseState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

PauseState*
PauseState::getSingletonPtr ()
{
    return msSingleton;
}

PauseState&
PauseState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void PauseState::createOverlay() {
    Ogre::LogManager::getSingletonPtr()->logMessage("=========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PauseState::createOverlay");
    Ogre::LogManager::getSingletonPtr()->logMessage("=========================");

    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    Ogre::Overlay *overlay = _overlayManager->getByName(OVERLAY_NAME);
    overlay->show();
}

void PauseState::manageOverlay() {
    if(_overlayManager != NULL){

        const char * filename = GameManager::getSingletonPtr()->getFicheroRecords();

        char cadena[128];
        std::ifstream fe(filename);
        if(fe){
            fe.getline(cadena,128);//first line is rubbish

            // First record
            Ogre::OverlayElement *oe;
            oe = _overlayManager->getOverlayElement("firstRecord");
            fe.getline(cadena,128);

            std::string st = cadena;
            std::vector<std::string> stSplited = split(st, '+');

            Ogre::String line;
            line.append(stSplited[0]);
            line.append(LEVEL_BROKEN_BRICKS_SEPARATOR);
            line.append(stSplited[1]);
            line.append(BROKEN_BRICKS_VAUS);
            line.append(stSplited[2]);

            oe->setCaption(line);

            // Second record
            oe = _overlayManager->getOverlayElement("secondRecord");
            fe.getline(cadena,128);

            st = cadena;
            stSplited = split(st, '+');

            line.clear();
            line.append(stSplited[0]);
            line.append(LEVEL_BROKEN_BRICKS_SEPARATOR);
            line.append(stSplited[1]);
            line.append(BROKEN_BRICKS_VAUS);
            line.append(stSplited[2]);

            oe->setCaption(line);

            // Third record
            oe = _overlayManager->getOverlayElement("thirdRecord");
            fe.getline(cadena,128);

            st = cadena;
            stSplited = split(st, '+');

            line.clear();
            line.append(stSplited[0]);
            line.append(LEVEL_BROKEN_BRICKS_SEPARATOR);
            line.append(stSplited[1]);
            line.append(BROKEN_BRICKS_VAUS);
            line.append(stSplited[2]);

            oe->setCaption(line);

            fe.close();
        }else{
            Ogre::LogManager::getSingletonPtr()->logMessage("No se ha podido abrir el fichero");
        }
    }
}

std::vector<std::string> & PauseState::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> PauseState::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
