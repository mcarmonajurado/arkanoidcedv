#include "GameOverState.h"
#include "IntroState.h"

template<> GameOverState* Ogre::Singleton<GameOverState>::msSingleton = 0;

void
GameOverState::enter ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Entrando en GameOverState");
    Ogre::LogManager::getSingletonPtr()->logMessage("=========================");

    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    //_viewport = _root->getAutoCreatedWindow()->getViewport(0);
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    // Nuevo background colour.
    //_viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 0.0));

    _exitGame = false;

    _sceneMgr->getRootSceneNode()->setPosition(0,0,0);
    _sceneMgr->getRootSceneNode()->resetOrientation();

    /* Game Over screen */
    _camera->setPosition(Ogre::Vector3(0,0,50));
    _camera->lookAt(Ogre::Vector3(0,0,0));
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(10000);

    //_viewport->setBackgroundColour(Ogre::ColourValue(0.0,0.0,0.0));
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    _camera->setAspectRatio(width / height);


    /* First we need to define the Plane(external link) object itself,
     * which is done by supplying a normal and the distance from the origin*/
    Ogre::Plane gameOverPlane(Ogre::Vector3::UNIT_Z, 0);

    /* The createPlane(external link) member function takes in a Plane
     * definition and makes a mesh from the parameters.
     * This registers our plane for use */
    Ogre::Real planeWidth = 20;
    Ogre::Real planeHeight = 20;
    int xsegments = 1;
    int ysegments = 1;
    Ogre::Real uTile=1;
    Ogre::Real vTile=1;
    Ogre::MeshManager::getSingleton().createPlane("gameOverPlane",
                                                  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                                                  gameOverPlane,planeWidth,planeHeight,xsegments,ysegments,
                                                  true,1,uTile,vTile,
                                                  Ogre::Vector3::UNIT_Y);

    Ogre::SceneNode* gameOverNode = _sceneMgr->createSceneNode("gameOver");
    Ogre::Entity* gameOverPlaneEntity = _sceneMgr->createEntity("gameOverPlaneEntity", "gameOverPlane");
    gameOverPlaneEntity->setMaterialName("MaterialGameOver");
    gameOverNode->attachObject(gameOverPlaneEntity);

    _sceneMgr->getRootSceneNode()->addChild(gameOverNode);
}

void
GameOverState::exit ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Saliendo de GameOverState");
    Ogre::LogManager::getSingletonPtr()->logMessage("=========================");
}

void
GameOverState::pause ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Pausando GameOverState");
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
}

void
GameOverState::resume ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Continuando GameOverState");
    Ogre::LogManager::getSingletonPtr()->logMessage("=========================");
}

bool
GameOverState::frameStarted
(const Ogre::FrameEvent& evt)
{
    return true;
}

bool
GameOverState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
GameOverState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    return true;
}

void
GameOverState::keyPressed
(const OIS::KeyEvent &e) {
    // Tecla p --> Estado anterior.
    if (e.key == OIS::KC_SPACE) {
        changeState(IntroState::getSingletonPtr());
    }
}

void
GameOverState::keyReleased
(const OIS::KeyEvent &e)
{
    switch(e.key)
    {
    case OIS::KC_ESCAPE:
        _exitGame = true;
    default:
        break;
    }if (e.key == OIS::KC_P) {
        popState();
    }
}

void
GameOverState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
GameOverState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
GameOverState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

GameOverState*
GameOverState::getSingletonPtr ()
{
    return msSingleton;
}

GameOverState&
GameOverState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}
