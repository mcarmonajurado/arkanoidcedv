#include "IntroState.h"
#include "PlayState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void
IntroState::enter ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Entrando en IntroState");
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");

    _root = Ogre::Root::getSingletonPtr();

    if(NULL != _sceneMgr)
    _root->destroySceneManager(_sceneMgr);
    _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
    _sceneMgr->destroyAllCameras();// If we returns from another state.
    _camera = _sceneMgr->createCamera("IntroCamera");
    _root->getAutoCreatedWindow()->removeAllViewports();// If we returns from another state.
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    //_viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));

    _exitGame = false;

    /* Intro screen */
    _camera->setPosition(Ogre::Vector3(0,0,50));
    _camera->lookAt(Ogre::Vector3(0,0,0));
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(10000);

    //_viewport->setBackgroundColour(Ogre::ColourValue(0.0,0.0,0.0));
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    _camera->setAspectRatio(width / height);


    /* First we need to define the Plane(external link) object itself,
     * which is done by supplying a normal and the distance from the origin*/
    Ogre::Plane introPlane(Ogre::Vector3::UNIT_Z, 0);

    /* The createPlane(external link) member function takes in a Plane
     * definition and makes a mesh from the parameters.
     * This registers our plane for use */
    Ogre::Real planeWidth = 50;
    Ogre::Real planeHeight = 50;
    int xsegments = 1;
    int ysegments = 1;
    Ogre::Real uTile=1;
    Ogre::Real vTile=1;
    Ogre::MeshManager::getSingleton().createPlane("introPlane",
                                                  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                                                  introPlane,planeWidth,planeHeight,xsegments,ysegments,
                                                  true,1,uTile,vTile,
                                                  Ogre::Vector3::UNIT_Y);

    Ogre::SceneNode* introNode = _sceneMgr->createSceneNode("intro");
    Ogre::Entity* introPlaneEntity = _sceneMgr->createEntity("introPlaneEntity", "introPlane");
    introPlaneEntity->setMaterialName("MaterialIntro");
    introNode->attachObject(introPlaneEntity);

    _sceneMgr->getRootSceneNode()->addChild(introNode);
}

void
IntroState::exit()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Saliendo de IntroState");
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void
IntroState::pause ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Pausando IntroState");
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");
}

void
IntroState::resume ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Continuando IntroState");
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
}

bool
IntroState::frameStarted
(const Ogre::FrameEvent& evt)
{
    return true;
}

bool
IntroState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
IntroState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    return true;
}

void
IntroState::keyPressed
(const OIS::KeyEvent &e)
{
    // Transición al siguiente estado.
    // Espacio --> PlayState
    if (e.key == OIS::KC_SPACE) {
        changeState(PlayState::getSingletonPtr());
    }
}

void
IntroState::keyReleased
(const OIS::KeyEvent &e )
{
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void
IntroState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
IntroState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
IntroState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

IntroState*
IntroState::getSingletonPtr ()
{
    return msSingleton;
}

IntroState&
IntroState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}
