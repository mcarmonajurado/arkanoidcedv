#include "CreditsState.h"
#include "IntroState.h"

template<> CreditsState* Ogre::Singleton<CreditsState>::msSingleton = 0;

void
CreditsState::enter ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Entrando en CreditsState");
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");

    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    // Nuevo background colour.
    //_viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 0.0));

    _exitGame = false;

    _sceneMgr->getRootSceneNode()->setPosition(0,0,0);
    _sceneMgr->getRootSceneNode()->resetOrientation();

    /* Credits screen */
    _camera->setPosition(Ogre::Vector3(0,0,50));
    _camera->lookAt(Ogre::Vector3(0,0,0));
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(10000);

    //_viewport->setBackgroundColour(Ogre::ColourValue(0.0,0.0,0.0));
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    _camera->setAspectRatio(width / height);


    /* First we need to define the Plane(external link) object itself,
     * which is done by supplying a normal and the distance from the origin*/
    Ogre::Plane creditsPlane(Ogre::Vector3::UNIT_Z, 0);

    /* The createPlane(external link) member function takes in a Plane
     * definition and makes a mesh from the parameters.
     * This registers our plane for use */
    Ogre::Real planeWidth = 20;
    Ogre::Real planeHeight = 20;
    int xsegments = 1;
    int ysegments = 1;
    Ogre::Real uTile=1;
    Ogre::Real vTile=1;
    Ogre::MeshManager::getSingleton().createPlane("creditsPlane",
                                                  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                                                  creditsPlane,planeWidth,planeHeight,xsegments,ysegments,
                                                  true,1,uTile,vTile,
                                                  Ogre::Vector3::UNIT_Y);

    Ogre::SceneNode* creditsNode = _sceneMgr->createSceneNode("credits");
    Ogre::Entity* creditsPlaneEntity = _sceneMgr->createEntity("creditsPlaneEntity", "creditsPlane");
    creditsPlaneEntity->setMaterialName("MaterialCredits");
    creditsNode->attachObject(creditsPlaneEntity);

    _sceneMgr->getRootSceneNode()->addChild(creditsNode);
}

void
CreditsState::exit ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Saliendo de CreditsState");
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
}

void
CreditsState::pause ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Pausando CreditsState");
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");
}

void
CreditsState::resume ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Continuando CreditsState");
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
}

bool
CreditsState::frameStarted
(const Ogre::FrameEvent& evt)
{
    return true;
}

bool
CreditsState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
CreditsState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    return true;
}

void
CreditsState::keyPressed
(const OIS::KeyEvent &e) {
    // Tecla c --> Estado anterior.
//    if (e.key == OIS::KC_C) {
//        popState();
//    }
    if (e.key == OIS::KC_SPACE) {
        changeState(IntroState::getSingletonPtr());
    }
}

void
CreditsState::keyReleased
(const OIS::KeyEvent &e)
{
    switch(e.key)
    {
    case OIS::KC_ESCAPE:
        _exitGame = true;
    default:
        break;
    }
}

void
CreditsState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
CreditsState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
CreditsState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

CreditsState*
CreditsState::getSingletonPtr ()
{
    return msSingleton;
}

CreditsState&
CreditsState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}
