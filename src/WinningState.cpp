
#include "WinningState.h"
#include "IntroState.h"

template<> WinningState* Ogre::Singleton<WinningState>::msSingleton = 0;

void
WinningState::enter ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Entrando en WinningState");
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");

    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    //_viewport = _root->getAutoCreatedWindow()->getViewport(0);
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    // Nuevo background colour.
    //_viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 0.0));

    _exitGame = false;

    _sceneMgr->getRootSceneNode()->setPosition(0,0,0);
    _sceneMgr->getRootSceneNode()->resetOrientation();

    /* Winning screen */
    _camera->setPosition(Ogre::Vector3(0,0,50));
    _camera->lookAt(Ogre::Vector3(0,0,0));
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(10000);

    //_viewport->setBackgroundColour(Ogre::ColourValue(0.0,0.0,0.0));
    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    _camera->setAspectRatio(width / height);


    /* First we need to define the Plane(external link) object itself,
     * which is done by supplying a normal and the distance from the origin*/
    Ogre::Plane winningPlane(Ogre::Vector3::UNIT_Z, 0);

    /* The createPlane(external link) member function takes in a Plane
     * definition and makes a mesh from the parameters.
     * This registers our plane for use */
    Ogre::Real planeWidth = 20;
    Ogre::Real planeHeight = 20;
    int xsegments = 1;
    int ysegments = 1;
    Ogre::Real uTile=1;
    Ogre::Real vTile=1;
    Ogre::MeshManager::getSingleton().createPlane("winningPlane",
                                                  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                                                  winningPlane,planeWidth,planeHeight,xsegments,ysegments,
                                                  true,1,uTile,vTile,
                                                  Ogre::Vector3::UNIT_Y);

    Ogre::SceneNode* winningNode = _sceneMgr->createSceneNode("winning");
    Ogre::Entity* winningPlaneEntity = _sceneMgr->createEntity("winningPlaneEntity", "winningPlane");
    winningPlaneEntity->setMaterialName("MaterialWinning");
    winningNode->attachObject(winningPlaneEntity);

    _sceneMgr->getRootSceneNode()->addChild(winningNode);
}

void
WinningState::exit ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Saliendo de WinningState");
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
}

void
WinningState::pause ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Pausando WinningState");
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");
}

void
WinningState::resume ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Continuando WinningState");
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
}

bool
WinningState::frameStarted
(const Ogre::FrameEvent& evt)
{
    return true;
}

bool
WinningState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
WinningState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    return true;
}

void
WinningState::keyPressed
(const OIS::KeyEvent &e) {
    // Tecla p --> Estado anterior.
    if (e.key == OIS::KC_SPACE) {
        changeState(IntroState::getSingletonPtr());
    }
}

void
WinningState::keyReleased
(const OIS::KeyEvent &e)
{
    switch(e.key)
    {
    case OIS::KC_ESCAPE:
        _exitGame = true;
    default:
        break;
    }if (e.key == OIS::KC_P) {
        popState();
    }
}

void
WinningState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
WinningState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
WinningState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

WinningState*
WinningState::getSingletonPtr ()
{
    return msSingleton;
}

WinningState&
WinningState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}
