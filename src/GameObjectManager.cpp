#include <OGRE/Ogre.h>
#include "GameObjectManager.h"

// this is required to properly instantiate Ogre::Singleton
template<> GameObjectManager* Ogre::Singleton<GameObjectManager>::msSingleton = 0;

GameObjectManager::GameObjectManager()
{

}

GameObjectManager::~GameObjectManager()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================================");
    Ogre::LogManager::getSingletonPtr()->logMessage("GameObjectManager::~GameObjectManager");
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================================");

}

void GameObjectManager::destroyAll()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("GameObjectManager::destroyAll");
    Ogre::LogManager::getSingletonPtr()->logMessage("=============================");

    GameObjectList::iterator it = _gameObjectList.begin();
    GameObjectList::iterator it_end = _gameObjectList.end();

    while(it != it_end)
    {
        (*it)->removeFromScene();
        delete *it;
        _gameObjectList.erase(it++);
    }
}

void GameObjectManager::update(Ogre::Real time_step)
{
//    Ogre::LogManager::getSingletonPtr()->logMessage("=========================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("GameObjectManager::update");
//    Ogre::LogManager::getSingletonPtr()->logMessage("=========================");

    GameObjectList::iterator it = _gameObjectList.begin();
    GameObjectList::iterator it_end = _gameObjectList.end();

    while (it != it_end)
    {
        bool result = (*it)->updateInnerStatus(time_step);
        // when objects update method returns false we remove them from the scene and delete them from the list
        if (false == result)
        {
            (*it)->removeFromScene();
            delete *it;
            _gameObjectList.erase(it++);
        } else
            ++it;
    }
}
