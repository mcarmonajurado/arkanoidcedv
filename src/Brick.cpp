#include <OGRE/Ogre.h>
#include "Brick.h"
#include "Ball.h"

unsigned long int Brick::brikCounterName = 0;
const Ogre::String BRICK_NAME = "Brick";
const Ogre::String BRICK_MESH = "Cube100.mesh";
const Ogre::String BRICK_MATERIAL = "MaterialRed";

Brick::Brick(const Ogre::Vector3 &position, const Ogre::Vector3 &size, int hardness) : GameObject(BRICK_NAME, BRICK_MESH, BRICK_MATERIAL)
{
    _pos = position;
    _size = size;
    _hardness = hardness;
    _name = _name + Ogre::StringConverter::toString(brikCounterName++);
}

Brick::Brick(const Ogre::Vector3 &position, const Ogre::Vector3 &size, int hardness, const Ogre::String& materialName) : GameObject(BRICK_NAME, BRICK_MESH, materialName)
{
    _pos = position;
    _size = size;
    _hardness = hardness;
    _name = _name + Ogre::StringConverter::toString(brikCounterName++);
}


void Brick::addToScene(Ogre::SceneManager* scene_manager){

    Ogre::LogManager::getSingletonPtr()->logMessage("=================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Brick::addToScene");
    Ogre::LogManager::getSingletonPtr()->logMessage("=================");

    Ogre::Entity* entity = scene_manager->createEntity(_name,_meshName);

    entity->setMaterialName(_materialName);

    _sceneNode = scene_manager->getRootSceneNode()->createChildSceneNode(_name, _pos);
    _sceneNode->attachObject(entity);
    _sceneNode->setScale(_size/SCALE);
}


int Brick::onHitBall(Ball* b){

    Ogre::LogManager::getSingletonPtr()->logMessage("================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Brick::onHitBall");
    Ogre::LogManager::getSingletonPtr()->logMessage("================");

    _size.z	  -= _size.z / _hardness * b->getHardness();
    _hardness -= b->getHardness();
    _sceneNode->setScale(_size/SCALE);
    return _hardness;
}

bool Brick::updateInnerStatus(Ogre::Real time_step){

//    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("Brick::updateInnerStatus");
//    Ogre::LogManager::getSingletonPtr()->logMessage("========================");

    return (_hardness>0)? true:false;
}

void Brick::removeFromScene()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Brick::removeFromScene");
    Ogre::LogManager::getSingletonPtr()->logMessage("======================");

    if(_sceneNode)
    {
        _sceneNode->getParentSceneNode()->removeAndDestroyChild(_sceneNode->getName());
        _sceneNode = 0;
    }
}

