#include <OGRE/Ogre.h>
#include "Ball.h"
#include "Paddle.h"
#include "Brick.h"

const Ogre::Vector3 Ball::STARTING_POSS  = Ogre::Vector3(0,-75,0);
const Ogre::Vector3 Ball::STARTING_SPEED = Ogre::Vector3(1,1,0) * 40;
const Ogre::Real	Ball::RADIUS		 = 3;
const Ogre::String BALL_NAME = "Ball";
const Ogre::String BALL_MESH = "Sphere.mesh";
const Ogre::String BALL_MATERIAL = "MaterialWhite";

Ball::Ball() : GameObject(BALL_NAME, BALL_MESH, BALL_MATERIAL)
{
    _hardness = 1;
    _speed = Ball::STARTING_SPEED;
    _startingPosition = STARTING_POSS;
    _collided = false;
}

Ball::Ball(const Ogre::Vector3 &pos, const Ogre::Vector3 &spd, int hardness) : GameObject(BALL_NAME, BALL_MESH, BALL_MATERIAL)
{
    _hardness = hardness;
    _speed = spd;
    _startingPosition = pos;
    _collided = false;
}

Ball::~Ball()
{

}

void Ball::removeFromScene()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Ball::removeFromScene");
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");

    if (_sceneNode)
    {
        _sceneNode->getParentSceneNode()->removeAndDestroyChild(_sceneNode->getName());
        _sceneNode = 0;
    }
}
void Ball::addToScene(Ogre::SceneManager* scene_manager)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Ball::addToScene");
    Ogre::LogManager::getSingletonPtr()->logMessage("================");

    Ogre::Entity *entity = scene_manager->createEntity(_name,_meshName);
    entity->setMaterialName(_materialName);

    _sceneNode = scene_manager->getRootSceneNode()->createChildSceneNode(_name, _startingPosition);
    _sceneNode->attachObject(entity);
    _sceneNode->setScale(Ogre::Vector3(RADIUS,RADIUS,RADIUS));
}

void Ball::onHitBrick(Brick *brick)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Ball::onHitBrick");
    Ogre::LogManager::getSingletonPtr()->logMessage("================");

    if (sideCollision(brick->getSceneNode()->getPosition(), brick->getSceneNode()->getScale() * SCALE))
        _speed.x *= -1;
    else
        _speed.y *= -1;
    _sceneNode->setPosition(_oldPosition);
}

void Ball::onHitWall()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("===============");
    Ogre::LogManager::getSingletonPtr()->logMessage("Ball::onHitWall");
    Ogre::LogManager::getSingletonPtr()->logMessage("===============");

    _speed.x *= -1;
    _sceneNode->setPosition(_oldPosition);
}

void Ball::onHitCeiling()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Ball::onHitCeiling");
    Ogre::LogManager::getSingletonPtr()->logMessage("==================");

    _speed.y *= -1;
    _sceneNode->setPosition(_oldPosition);
}

void Ball::onHitPaddle(Paddle *pad)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Ball::onHitPaddle");
    Ogre::LogManager::getSingletonPtr()->logMessage("=================");

    if (sideCollision(pad->getSceneNode()->getPosition(), pad->getSceneNode()->getScale() * SCALE))
        _speed.x *= -1;
    else
    {
        _speed.y *= -1;
        _speed.x += _sceneNode->getPosition().x - pad->getSceneNode()->getPosition().x;
    }
}

bool Ball::sideCollision(const Ogre::Vector3& rectangle_position, const Ogre::Vector3& rectangle_size)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Ball::sideCollision");
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");

    bool collision = false;

    if((_sceneNode->getPosition().y >= rectangle_position.y - rectangle_size.y / 2 - _sceneNode->getScale().y / 2)
            && (_sceneNode->getPosition().y <= rectangle_position.y + rectangle_size.y / 2 + _sceneNode->getScale().y / 2))
        collision = true;

    return collision;
}
