#include <iostream>
#include <fstream>

#include <string.h>
#include <sstream>

#include "PlayState.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "Levels.h"
#include "GameObject.h"
#include "CreditsState.h"
#include "WinningState.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

const Ogre::Real PlayState::PADDLE_VELOCITY = 100;
//const Ogre::Real PlayState::LENGTH_PENALTY = 5;
const Ogre::Real PlayState::LENGTH_PENALTY = 0;

const Ogre::Real PlayState::LEFT_WALL = -108;
const Ogre::Real PlayState::RIGHT_WALL = 110;
const Ogre::Real PlayState::CEILING = 80;
const Ogre::Real PlayState::FLOOR = -90;

const Ogre::String OVERLAY_NAME = "Info";
const Ogre::String SONG = "17_encore_303_vbr.mp3";
const Ogre::String SOUND_EFFECT_HIT = "93550__sagitaurius__hit-086.audacity.wav";
const Ogre::String SOUND_EFFECT_DESTROY = "33245__ljudman__grenade.audacity.wav";

static const Ogre::ColourValue BACKGROUND_COLOUR = Ogre::ColourValue(0.0, 0.0, 0.3);

static const int SCALE_1 = 100;

static const Ogre::String TEXT_RECORD_CREATION = "Nunca has ganado";
static const Ogre::String LEVEL_BROKEN_BRICKS_SEPARATOR = "+";
static const Ogre::String BROKEN_BRICKS_VAUS = "+";

void
PlayState::enter ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Entrando en PlayState");
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");

    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    // Nuevo background colour.
    _viewport->setBackgroundColour(BACKGROUND_COLOUR);

    _exitGame = false;

    // Carga del sonido.
    _mainTrack = _pTrackManager->load(SONG);
    _simpleEffect = _pSoundFXManager->load(SOUND_EFFECT_HIT);
    _simpleEffectDestroy = _pSoundFXManager->load(SOUND_EFFECT_DESTROY);

    // Reproducción del track principal...
    this->_mainTrack->play();

    // Juego
    _camera->setPosition( Ogre::Vector3( 0, 0, 300 ) ); // set Camera position
    _camera->lookAt( Ogre::Vector3( 0, 0, 0 ) ); // set where Camera looks
    _camera->setNearClipDistance( 5 ); // near distance Camera can see
    _camera->setFarClipDistance( 1000 ); // far distance Camera can see

    _camera->setAspectRatio(
                Ogre::Real(_viewport->getActualWidth()) / Ogre::Real(_viewport->getActualHeight()));

    _sceneMgr->setAmbientLight( Ogre::ColourValue( 0.75, 0.75, 0.75 ) );

    Ogre::Light *light = _sceneMgr->createLight( "Light" );
    light->setType(Ogre::Light::LT_POINT);
    light->setSpecularColour(0.1,0.3,0.8);
    light->setPosition( 0, 0, 55 );
    light->setCastShadows(true);

    _gameObjectManager = new GameObjectManager();

    _level = new Levels();
    _level->createLevel(_sceneMgr);// We create a level of type 1 by default

    _ball = new Ball();
    GameObjectManager::getSingletonPtr()->addObjectToCollection(_ball);
    _paddle = new Paddle(LEFT_WALL, RIGHT_WALL);
    GameObjectManager::getSingletonPtr()->addObjectToCollection(_paddle);
    _ball->addToScene(_sceneMgr);
    _paddle->addToScene(_sceneMgr);
    _paddleCollided = false;

    _sceneMgr->getRootSceneNode()->setPosition(0,0,0);
    _sceneMgr->getRootSceneNode()->resetOrientation();
    _sceneMgr->getRootSceneNode()->pitch(Ogre::Degree(-45));

    createOverlay();
}

void
PlayState::exit ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Saliendo de PlayState");
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");

    delete _gameObjectManager;
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();

    // Detención del track principal...
    _mainTrack->stop();
    _simpleEffect->stop();

    _overlayManager->getByName(OVERLAY_NAME)->hide();
    //_overlayManager->destroyAll();
}

void
PlayState::pause()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Pausando PlayState");
    Ogre::LogManager::getSingletonPtr()->logMessage("==================");

    // Detención del track principal...
    _mainTrack->stop();
    _simpleEffect->stop();
}

void
PlayState::resume()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Continuando PlayState");
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");

    // Se restaura el background colour.
    _viewport->setBackgroundColour(BACKGROUND_COLOUR);

    // Reproducción del track principal...
    this->_mainTrack->play();
}

bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
    // Gestion del overlay
    if(_overlayManager != NULL){
        Ogre::OverlayElement *oe;
        oe = _overlayManager->getOverlayElement("vausInfo");
        oe->setCaption(Ogre::StringConverter::toString(GameManager::getSingletonPtr()->getLife()));
        oe = _overlayManager->getOverlayElement("bricksInfo");
        oe->setCaption(Ogre::StringConverter::toString(GameManager::getSingletonPtr()->getBrokenBricks()));
        oe = _overlayManager->getOverlayElement("lastLeveInfo");
        oe->setCaption(Ogre::StringConverter::toString(GameManager::getSingletonPtr()->getLastLevel()));
    }

    return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
PlayState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("===============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::frameRenderingQueued");
    Ogre::LogManager::getSingletonPtr()->logMessage("===============================");

    if(_level!=NULL && GameObjectManager::getSingletonPtr()!=NULL && !GameObjectManager::getSingletonPtr()->isEmpty()){
        if (true == _level->getLevelBrickList().empty())
        {
            int lastLevel = GameManager::getSingletonPtr()->getLastLevel();
            if(lastLevel<=_level->HIGHEST_LEVEL){
                GameManager::getSingletonPtr()->setLastLevel(lastLevel+1);
                lastLevel = GameManager::getSingletonPtr()->getLastLevel();
                _level->createLevel(_sceneMgr, lastLevel);
                Ogre::LogManager::getSingletonPtr()->logMessage("new level of type " + Ogre::StringConverter::toString(lastLevel) + " created");
            }else{
                changeState(WinningState::getSingletonPtr());
                return true;
            }
        }

        checkCollisions();
        if(GameObjectManager::getSingletonPtr() != NULL)
            GameObjectManager::getSingletonPtr()->update(evt.timeSinceLastFrame);

        return true;
    } else {
        popState();
        return true;
    }
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
    switch(e.key)
    {
    case OIS::KC_P:// Tecla p --> PauseState.
        pushState(PauseState::getSingletonPtr());
        break;
    case OIS::KC_SPACE:// resetea el juego
        changeState(PlayState::getSingletonPtr());
        break;
    case OIS::KC_RIGHT:
        _paddle->setSpeed(PADDLE_VELOCITY);
        break;
    case OIS::KC_LEFT:
        _paddle->setSpeed(-PADDLE_VELOCITY);
        break;
    case OIS::KC_B:
        _ball->setSpeed(_ball->getSpeed()/1.10);
        break;
    case OIS::KC_V:
        _ball->setSpeed(_ball->getSpeed()*1.10);
        break;
    case OIS::KC_X:
        _sceneMgr->getRootSceneNode()->pitch(Ogre::Degree(45));
        break;
    case OIS::KC_Y:
        _sceneMgr->getRootSceneNode()->yaw(Ogre::Degree(45));
        break;
    case OIS::KC_Z:
        _sceneMgr->getRootSceneNode()->roll(Ogre::Degree(45));
        break;
    case OIS::KC_C:
        //pushState(CreditsState::getSingletonPtr());
        changeState(CreditsState::getSingletonPtr());
        break;
    case OIS::KC_0:
        _sceneMgr->getRootSceneNode()->setPosition(0,0,0);
        _sceneMgr->getRootSceneNode()->resetOrientation();
        break;
    default:
        break;
    }
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
    switch(e.key)
    {
    case OIS::KC_ESCAPE:
        saveRecord();
        _exitGame = true;
        break;
    case OIS::KC_RIGHT:
        _paddle->setSpeed(0);
        break;
    case OIS::KC_LEFT:
        _paddle->setSpeed(0);
        break;
    default:
        break;
    }
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

PlayState*
PlayState::getSingletonPtr ()
{
    return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void PlayState::checkCollisions()
{
//    Ogre::LogManager::getSingletonPtr()->logMessage("==========================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::checkCollisions");
//    Ogre::LogManager::getSingletonPtr()->logMessage("==========================");


    Ogre::LogManager::getSingletonPtr()->logMessage("Part 1: brick check");
    checkBrickCollisions();

    Ogre::LogManager::getSingletonPtr()->logMessage("Part 2: paddle check");
    checkPaddleCollisions();

    Ogre::LogManager::getSingletonPtr()->logMessage("Part 3: borders check");
    checkBordersCollisions();

    Ogre::LogManager::getSingletonPtr()->logMessage("End collision check");
}

void PlayState::checkBrickCollisions()
{
//    Ogre::LogManager::getSingletonPtr()->logMessage("===============================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::checkBrickCollisions");
//    Ogre::LogManager::getSingletonPtr()->logMessage("===============================");

    // We check if the ball collide with the bricks
    BrickList &brickList = _level->getLevelBrickList();
    BrickList::iterator it = brickList.begin();
    BrickList::iterator it_end = brickList.end();

    while(it != it_end)
    {
        if (circleCollidesRectangle(_ball->getSceneNode()->getPosition(),
                                            _ball->getSceneNode()->getScale().x,
                                            (*it)->getSceneNode()->getPosition(),
                                            (*it)->getSceneNode()->getScale() * SCALE_1))
        {
            _ball->onHitBrick((*it));
            if ((*it)->onHitBall(_ball)<=0){
                brickList.erase(it++);
                GameManager::getSingletonPtr()->setBrokenBricks(GameManager::getSingletonPtr()->getBrokenBricks()+1);
                this->_simpleEffectDestroy->play();
            }else{
                this->_simpleEffect->play();
            }
        } else
            ++it;
    }
}

void PlayState::checkPaddleCollisions()
{
//    Ogre::LogManager::getSingletonPtr()->logMessage("================================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::checkPaddleCollisions");
//    Ogre::LogManager::getSingletonPtr()->logMessage("================================");

    // We check if the ball collide with the paddle
    if (circleCollidesRectangle(_ball->getSceneNode()->getPosition(),
                                        _ball->getSceneNode()->getScale().x,
                                        _paddle->getSceneNode()->getPosition(),
                                        _paddle->getSceneNode()->getScale() * SCALE_1))
    {
        if (!_paddleCollided)
            _ball->onHitPaddle(_paddle);
        _paddleCollided = true;
    } else
        _paddleCollided = false;
}

void PlayState::checkBordersCollisions()
{
//    Ogre::LogManager::getSingletonPtr()->logMessage("=================================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::checkBordersCollisions");
//    Ogre::LogManager::getSingletonPtr()->logMessage("=================================");

    // We check if the ball collide with the borders
    // Cheking the walls
    if ((_ball->getSceneNode()->getPosition().x < LEFT_WALL) ||
            _ball->getSceneNode()->getPosition().x > RIGHT_WALL)
        _ball->onHitWall();
    else
        // Cheking the ceiling
        if(_ball->getSceneNode()->getPosition().y > CEILING)
            _ball->onHitCeiling();
        else
            // Cheking the floor
            if(_ball->getSceneNode()->getPosition().y < FLOOR)
            {
                if (_paddle->getLength() > 0)
                {
                    _paddle->SetLength(_paddle->getLength() - LENGTH_PENALTY);
                } else
                    _paddle->SetLength(0);

                // Now we position the ball int a start point
                //_ball->getSceneNode()->setPosition(Ogre::Vector3::ZERO);
                _ball->getSceneNode()->setPosition(_paddle->getSceneNode()->getPosition());
                Ogre::Vector3 speed = _ball->getSpeed();
                speed.y *= -1;
                speed.x = _paddle->getSceneNode()->getPosition().x;
                _ball->setSpeed(speed);
                GameManager::getSingletonPtr()->setLife(GameManager::getSingletonPtr()->getLife()-1);
                if(GameManager::getSingletonPtr()->getLife()<=0){
                    _gameObjectManager->destroyAll();
                    saveRecord();
                    changeState(GameOverState::getSingletonPtr());
                    //pushState(GameOverState::getSingletonPtr());
                }
            }
}

bool PlayState::circleCollidesRectangle(const Ogre::Vector3& circle_position, Ogre::Real circle_radius,
                                   const Ogre::Vector3& rectangle_position, const Ogre::Vector3& rectangle_size)
{
//    Ogre::LogManager::getSingletonPtr()->logMessage("==================================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::CircleCollidesRectangle");
//    Ogre::LogManager::getSingletonPtr()->logMessage("==================================");

    bool collision = false;

    //distance ball - brick/paddle
    Ogre::Real circle_distance_x = std::abs(circle_position.x - rectangle_position.x);
    Ogre::Real circle_distance_y = std::abs(circle_position.y - rectangle_position.y);

    //First, we check the non collision cases
    if (circle_distance_x > (rectangle_size.x/2 + circle_radius)) {
        collision = false;

    } else
        if (circle_distance_y > (rectangle_size.y/2 + circle_radius)) {
            collision = false;

        } else{
            //Now, we check the collision cases
            if (circle_distance_x <= (rectangle_size.x/2)) {
                collision = true;
            } else
                if (circle_distance_y <= (rectangle_size.y/2)) {
                    collision = true;
                } else {
                    //here we check the diagonal
                    Ogre::Real corner_distance_sq = std::pow((circle_distance_x - rectangle_size.x/2),2) +
                            std::pow((circle_distance_y - rectangle_size.y/2),2);

                    collision = (corner_distance_sq <= std::pow(circle_radius,2));
                }
        }

    return collision;
}

void PlayState::createOverlay() {
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::createOverlay");
    Ogre::LogManager::getSingletonPtr()->logMessage("========================");

    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    Ogre::Overlay *overlay = _overlayManager->getByName(OVERLAY_NAME);
    overlay->show();
}

void PlayState::saveRecord() {
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::saveRecord");
    Ogre::LogManager::getSingletonPtr()->logMessage("=====================");

    const char * filename = GameManager::getSingletonPtr()->getFicheroRecords();

    // I try to open de record file
    std::ifstream fe(filename);
    if(!fe){
        Ogre::LogManager::getSingletonPtr()->logMessage("El fichero no existe");
        // If it doesn't exist, I create it
        std::ofstream fs(filename);
        fs << TEXT_RECORD_CREATION << std::endl;
        fs.close();
    }else{
        Ogre::LogManager::getSingletonPtr()->logMessage("El fichero existe");
        fe.close();
    }

    // Line format: Level+brokenBricks+life
    Ogre::String line;
    line.append(Ogre::StringConverter::toString(GameManager::getSingletonPtr()->getLastLevel()));
    line.append(LEVEL_BROKEN_BRICKS_SEPARATOR);
    line.append(Ogre::StringConverter::toString(GameManager::getSingletonPtr()->getBrokenBricks()));
    line.append(BROKEN_BRICKS_VAUS);
    line.append(Ogre::StringConverter::toString(GameManager::getSingletonPtr()->getLife()));

    char cadena[128];
    //std::fstream fio(filename);
    std::fstream fio(filename, std::ios_base::app | std::ios_base::in | std::ios_base::out);
    if(fio){
        fio.getline(cadena,128);//first line is rubbish
        //TODO MCJ hacer que solo se guarden las tres mejores

        /*
        fio.getline(cadena,128);
        std::string st = cadena;
        std::vector<std::string> stSplited = split(st, '+');
        std::cout << stSplited[0] << std::endl;
        std::cout << stSplited[1] << std::endl;
        std::cout << stSplited[2] << std::endl;

        unsigned long ul1 = strtoul (stSplited[0].data(), NULL, 0);
        unsigned long ul2 = strtoul (stSplited[1].data(), NULL, 0);
        unsigned long ul3 = strtoul (stSplited[2].data(), NULL, 0);
        Tengo que comparar los elementos y ordenadar según criterio
        */


        fio << line << std::endl;
        Ogre::LogManager::getSingletonPtr()->logMessage("Fichero de récords cambiado");
    }else{
        Ogre::LogManager::getSingletonPtr()->logMessage("No se ha podido abrir el fichero para escribir");
    }
    fio.close();

}

std::vector<std::string> & PlayState::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> PlayState::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
