#include <OGRE/Ogre.h>
#include "Levels.h"
#include "Brick.h"
#include "GameObjectManager.h"
#include "GameManager.h"

//const int HIGHEST_LEVEL = 2;

static const Ogre::String MATERIALS[] = {
    "MaterialGreen",
    "MaterialGrey",
    "MaterialRed",
    "MaterialWhite",
    "MaterialYellow",
    "MaterialBlue",
};

static const int MATERIALS_SIZE = 6;

void Levels::createLevel(Ogre::SceneManager *scene_manager){

    Ogre::LogManager::getSingletonPtr()->logMessage("===================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Levels::createLevel");
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");

    createLevel1(scene_manager);
}

void Levels::createLevel(Ogre::SceneManager *scene_manager, int level){

    Ogre::LogManager::getSingletonPtr()->logMessage("===================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Levels::createLevel");
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");

    switch(level){
    case 1  :
        createLevel1(scene_manager);
        break;
    case 2  :
        createLevel2(scene_manager);
        break;
    default :
        Ogre::LogManager::getSingletonPtr()->logMessage("Not existing level");
        break;
    }
}

void Levels::createLevel1(Ogre::SceneManager *scene_manager){
    GameManager::getSingletonPtr()->setLife(3);
    GameManager::getSingletonPtr()->setBrokenBricks(0);
    GameManager::getSingletonPtr()->setLastLevel(1);
    int rowNum = 5;
    int colNum = 10;
    int high = 8;
    int width = 20;

    Ogre::Real posX;
    Ogre::Real posY;
    Ogre::Real posZ = 0;

    int row;// row iterator
    int col;// column iterator
    int hardness;

    for(row=0;row<rowNum;row++)
        for(col=0;col<colNum;col++)
        {
            hardness = 1;// how many hits to destroy it
            posX = -100 + col*22.5;
            posY = row*10 + 30;
            Ogre::String material = MATERIALS[row%5];

            Brick *current_brick = new Brick(Ogre::Vector3(posX,posY,posZ),Ogre::Vector3(width,high,hardness + 1), hardness, material);
            _bricksList.push_back(current_brick);
            GameObjectManager::getSingleton().addObjectToCollection(current_brick);
            current_brick->addToScene(scene_manager);
        }
    Brick *current_brick = new Brick(Ogre::Vector3(0,80,0),Ogre::Vector3(230,6,6), 1, "MaterialGrey");
    current_brick->addToScene(scene_manager);

    current_brick = new Brick(Ogre::Vector3(-116,0,0),Ogre::Vector3(6,166,6), 1, "MaterialGrey");
    current_brick->addToScene(scene_manager);

    current_brick = new Brick(Ogre::Vector3(118,0,0),Ogre::Vector3(6,166,6), 1, "MaterialGrey");
    current_brick->addToScene(scene_manager);
}

void Levels::createLevel2(Ogre::SceneManager *scene_manager){
    GameManager::getSingletonPtr()->setLife(3);
    GameManager::getSingletonPtr()->setBrokenBricks(0);
    GameManager::getSingletonPtr()->setLastLevel(2);
    int rowNum = 5;
    int colNum = 10;
    int high = 8;
    int width = 20;

    Ogre::Real posX;
    Ogre::Real posY;
    Ogre::Real posZ = 0;

    int row;// row iterator
    int col;// column iterator
    int hardness;

    for(row=0;row<rowNum;row++)
        for(col=0;col<colNum;col++)
        {
            hardness = (row>col)? row-col+1:col-row+1;// how many hits to destroy it
            posX = -100 + col*22.5;
            posY = row*10 + 30;
            Ogre::String material = MATERIALS[row%5];

            Brick *current_brick = new Brick(Ogre::Vector3(posX,posY,posZ),Ogre::Vector3(width,high,hardness + 1), hardness, material);
            _bricksList.push_back(current_brick);
            GameObjectManager::getSingleton().addObjectToCollection(current_brick);
            current_brick->addToScene(scene_manager);
        }
}
