#include <OGRE/Ogre.h>
#include "Paddle.h"

const Ogre::Vector3 Paddle::STARTING_POSITION = Ogre::Vector3(0,-75,0);
const Ogre::Vector3 Paddle::STARTING_SIZE	  = Ogre::Vector3(30,5,5);

const Ogre::String PADDLE_NAME = "Paddle";
const Ogre::String PADDLE_MESH = "Sphere100.mesh";
const Ogre::String PADDLE_MATERIAL = "MaterialRed";

Paddle::Paddle() : GameObject(PADDLE_NAME, PADDLE_MESH, PADDLE_MATERIAL), _length(STARTING_SIZE.x), _speedX(0)
{

}

Paddle::Paddle(Ogre::Real length) : GameObject(PADDLE_NAME, PADDLE_MESH, PADDLE_MATERIAL), _length(length), _speedX(0)
{

}

Paddle::Paddle(Ogre::Real leftLimit, Ogre::Real rightLimit) : GameObject(PADDLE_NAME, PADDLE_MESH, PADDLE_MATERIAL), _length(STARTING_SIZE.x), _speedX(0), _leftLimit(leftLimit), _rightLimit(rightLimit)
{

}

Paddle::~Paddle()
{

}

void Paddle::addToScene(Ogre::SceneManager* scene_manager)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Paddle::addToScene");
    Ogre::LogManager::getSingletonPtr()->logMessage("==================");

    Ogre::Entity* entity = scene_manager->createEntity(_name,_meshName);
    entity->setMaterialName(_materialName);

    _sceneNode = scene_manager->getRootSceneNode()->createChildSceneNode(_name, STARTING_POSITION);
    _sceneNode->attachObject(entity);

    Ogre::Vector3 size(STARTING_SIZE);
    size.x = _length;

    _sceneNode->setScale(STARTING_SIZE/SCALE);
}

void Paddle::removeFromScene()
{
   Ogre::LogManager::getSingletonPtr()->logMessage("=======================");
   Ogre::LogManager::getSingletonPtr()->logMessage("Paddle::removeFromScene");
   Ogre::LogManager::getSingletonPtr()->logMessage("=======================");

    if(_sceneNode)
    {
        _sceneNode->getParentSceneNode()->removeAndDestroyChild(_sceneNode->getName());
        _sceneNode = 0;
    }
}

bool Paddle::updateInnerStatus(Ogre::Real time_step)
{
//   Ogre::LogManager::getSingletonPtr()->logMessage("=========================");
//   Ogre::LogManager::getSingletonPtr()->logMessage("Paddle::updateInnerStatus");
//   Ogre::LogManager::getSingletonPtr()->logMessage("=========================");

    if(
            (((_sceneNode->getPosition().x + _sceneNode->getScale().x * SCALE/2) <= _rightLimit) && _speedX > 0)
            ||
            (((_sceneNode->getPosition().x - _sceneNode->getScale().x * SCALE/2) >= _leftLimit) && _speedX < 0)
            )
        _sceneNode->translate( Ogre::Vector3(_speedX * time_step,0,0) );
    return true;
}
