#include <OGRE/Ogre.h>

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include "GameManager.h"
#include "GameState.h"

template<> GameManager* Ogre::Singleton<GameManager>::msSingleton = 0;
const char* GameManager::_ficheroRecords = "records.txt";
const char* GameManager::TITLE = "Arkanoid";

GameManager::GameManager ()
{
    std::cout << "===================================================" << std::endl;
    std::cout << "GameManager::GameManager - Iniciando el GameManager" << std::endl;
    std::cout << "===================================================" << std::endl;

    _root = 0;

    //sonido
    initSDL();
}

GameManager::~GameManager ()
{
    std::cout << "=====================================================" << std::endl;
    std::cout << "GameManager::~GameManager - Eliminando el GameManager" << std::endl;
    std::cout << "=====================================================" << std::endl;

    while (!_states.empty()) {
        _states.top()->exit();
        _states.pop();
    }

    if (_root)
        delete _root;
}

void
GameManager::start
(GameState* state)
{
    std::cout << "==================" << std::endl;
    std::cout << "GameManager::start" << std::endl;
    std::cout << "==================" << std::endl;

    // Creación del objeto Ogre::Root.
    _root = new Ogre::Root();

    // Sonido
    _pTrackManager = new TrackManager;
    _pSoundFXManager = new SoundFXManager;

    loadResources();

    if (!configure())
        return;

    _inputMgr = new InputManager;
    _inputMgr->initialise(_renderWindow);

    // Registro como key y mouse listener...
    _inputMgr->addKeyListener(this, "GameManager");
    _inputMgr->addMouseListener(this, "GameManager");

    // El GameManager es un FrameListener.
    _root->addFrameListener(this);

    // Transición al estado inicial.
    changeState(state);

    // Bucle de rendering.
    _root->startRendering();
}

void
GameManager::changeState
(GameState* state)
{
    std::cout << "========================" << std::endl;
    std::cout << "GameManager::changeState" << std::endl;
    std::cout << "========================" << std::endl;

    // Limpieza del estado actual.
    if (!_states.empty()) {
        // exit() sobre el último estado.
        _states.top()->exit();
        // Elimina el último estado.
        _states.pop();
    }

    // Transición al nuevo estado.
    _states.push(state);
    // pasamos los manejadores de sonido al siguiente estado.
    _states.top()->setTrackManager(_pTrackManager);
    _states.top()->setSoundFXManager(_pSoundFXManager);
    // enter() sobre el nuevo estado.
    _states.top()->enter();
}

void
GameManager::pushState
(GameState* state)
{
    std::cout << "======================" << std::endl;
    std::cout << "GameManager::pushState" << std::endl;
    std::cout << "======================" << std::endl;

    // Pausa del estado actual.
    if (!_states.empty())
        _states.top()->pause();

    // Transición al nuevo estado.
    _states.push(state);
    // enter() sobre el nuevo estado.
    _states.top()->enter();
}

void
GameManager::popState ()
{
    std::cout << "=====================" << std::endl;
    std::cout << "GameManager::popState" << std::endl;
    std::cout << "=====================" << std::endl;

    // Limpieza del estado actual.
    if (!_states.empty()) {
        _states.top()->exit();
        _states.pop();
    }

    // Vuelta al estado anterior.
    if (!_states.empty())
        _states.top()->resume();
}

void
GameManager::loadResources ()
{
    std::cout << "==========================" << std::endl;
    std::cout << "GameManager::loadResources" << std::endl;
    std::cout << "==========================" << std::endl;

    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while (sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typestr = i->first;    datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation
                    (datastr, typestr, sectionstr);
        }
    }
}

bool
GameManager::configure ()
{
    std::cout << "======================" << std::endl;
    std::cout << "GameManager::configure" << std::endl;
    std::cout << "======================" << std::endl;

    if (!_root->restoreConfig()) {
        if (!_root->showConfigDialog()) {
            return false;
        }
    }

    _renderWindow = _root->initialise(true, TITLE);

    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    return true;
}

GameManager*
GameManager::getSingletonPtr ()
{
    return msSingleton;
}

GameManager&
GameManager::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

// Las siguientes funciones miembro delegan
// el evento en el estado actual.
bool
GameManager::frameStarted
(const Ogre::FrameEvent& evt)
{
    _inputMgr->capture();
    return _states.top()->frameStarted(evt);
}

bool
GameManager::frameEnded
(const Ogre::FrameEvent& evt)
{
    return _states.top()->frameEnded(evt);
}

bool
GameManager::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    return _states.top()->frameRenderingQueued(evt);
}

bool
GameManager::keyPressed
(const OIS::KeyEvent &e)
{
    _states.top()->keyPressed(e);
    return true;
}

bool
GameManager::keyReleased
(const OIS::KeyEvent &e)
{
    _states.top()->keyReleased(e);
    return true;
}

bool
GameManager::mouseMoved
(const OIS::MouseEvent &e)
{
    _states.top()->mouseMoved(e);
    return true;
}

bool
GameManager::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    _states.top()->mousePressed(e, id);
    return true;
}

bool
GameManager::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    _states.top()->mouseReleased(e, id);
    return true;
}

bool GameManager::initSDL ()
{
    std::cout << "===================================" << std::endl;
    std::cout << "GameManager::initSDL: Iniciando SDL" << std::endl;
    std::cout << "===================================" << std::endl;

    // Inicializando SDL...
    if (SDL_Init(SDL_INIT_AUDIO) < 0)
        return false;
    // Llamar a  SDL_Quit al terminar.
    atexit(SDL_Quit);

    // Inicializando SDL mixer...
    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0)
        return false;

    // Llamar a Mix_CloseAudio al terminar.
    atexit(Mix_CloseAudio);

    return true;
}
